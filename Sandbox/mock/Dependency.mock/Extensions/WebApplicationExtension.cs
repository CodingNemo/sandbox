﻿
using Dependency.mock.Extensions;

namespace Microsoft.AspNetCore.Routing;

public static class WebApplicationExtension
{
    public static IEndpointRouteBuilder StubRoute(this IEndpointRouteBuilder endpoints, HttpMethod method, string pattern, Action<EndPointRouteStubBuilder> handler
    )
    {
        var builder = new EndPointRouteStubBuilder(method, pattern);

        handler(builder);

        builder.StubEndpoint(endpoints);

        return endpoints;
    }

    public static IEndpointRouteBuilder StubGet(this IEndpointRouteBuilder endpoints, string pattern,
        Action<EndPointRouteStubBuilder> handler
    ) => endpoints.StubRoute(HttpMethod.Get, pattern, handler);

    public static IEndpointRouteBuilder StubPost(this IEndpointRouteBuilder endpoints, string pattern,
        Action<EndPointRouteStubBuilder> handler
    ) => endpoints.StubRoute(HttpMethod.Post, pattern, handler);

    public static IEndpointRouteBuilder StubPut(this IEndpointRouteBuilder endpoints, string pattern,
        Action<EndPointRouteStubBuilder> handler
    ) => endpoints.StubRoute(HttpMethod.Put, pattern, handler);

    public static IEndpointRouteBuilder StubPatch(this IEndpointRouteBuilder endpoints, string pattern,
        Action<EndPointRouteStubBuilder> handler
    ) => endpoints.StubRoute(HttpMethod.Patch, pattern, handler);

    public static IEndpointRouteBuilder StubDelete(this IEndpointRouteBuilder endpoints, string pattern,
        Action<EndPointRouteStubBuilder> handler
    ) => endpoints.StubRoute(HttpMethod.Delete, pattern, handler);
}