﻿using Dependency.mock.Extensions.Content;
using Dependency.mock.Extensions.Rule;

namespace Dependency.mock.Extensions;

public class EndPointRouteStubBuilder
{
    private readonly HttpMethod _httpMethod;
    private readonly string _pattern;

    private readonly Queue<StubRouteRule> _rules = new();

    public EndPointRouteStubBuilder(HttpMethod httpMethod, string pattern)
    {
        _httpMethod = httpMethod;
        _pattern = pattern;
    }

    public void AlwaysReturnFileContent(
        string filePath, int statusCode = StatusCodes.Status200OK)
    {
        _rules.Enqueue(StubRouteRule.When(_ => true).Returns(
                new StubResponseFromFile(filePath))
            .WithStatusCode(statusCode)
        );
    }

    public EndPointRouteStubBuilder ReturnFileContent(
        string filePath, Predicate<HttpContext> when, int statusCode = StatusCodes.Status200OK)
    {
        _rules.Enqueue(StubRouteRule.When(when)
            .Returns(new StubResponseFromFile(filePath))
            .WithStatusCode(statusCode));
        return this;
    }

    public void AlwaysReturnContent(
        object content, int statusCode = StatusCodes.Status200OK)
    {
        _rules.Enqueue(StubRouteRule.When(_ => true).Returns(
                new StubResponseFromObject(content))
            .WithStatusCode(statusCode)
        );
    }

    private StubRouteRule? FindRule(HttpContext context)
        => _rules.FirstOrDefault(rule => rule.Matches(context));

    private static readonly StubRouteRule DefaultRule = StubRouteRule.When(x => true)
        .Returns(new StubResponseFromObject(new
        {
            Message = "This route has not been stubbed."
        }))
        .WithStatusCode(StatusCodes.Status404NotFound);

    public void StubEndpoint(IEndpointRouteBuilder endpoints)
    {
        endpoints.Map(_pattern, async context =>
        {
            var rule = FindRule(context) ?? DefaultRule;
            await rule.Apply(context);
        }).WithMetadata(new HttpMethodMetadata(new []{ _httpMethod.Method }));
    }
}