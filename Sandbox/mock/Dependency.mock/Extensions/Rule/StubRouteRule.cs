﻿using System.Text;
using Dependency.mock.Extensions.Content;

namespace Dependency.mock.Extensions.Rule
{
    public class StubRouteRule
    {
        public Predicate<HttpContext> Condition { get; }
        public IStubResponseContent ResponseContent { get; }
        public int StatusCode { get; private set; }

        public class StubRouteRuleBuilder
        {
            private readonly Predicate<HttpContext> _condition;

            private int _statusCode = StatusCodes.Status200OK;

            private StubRouteRuleBuilder(Predicate<HttpContext> condition)
            {
                _condition = condition;
            }

            public static StubRouteRuleBuilder When(Predicate<HttpContext> condition) => new(condition);

        

            public StubRouteRule Returns(IStubResponseContent responseContent) => new(_condition, responseContent);

        }

        private StubRouteRule(Predicate<HttpContext> condition, IStubResponseContent responseContent)
        {
            Condition = condition;
            ResponseContent = responseContent;
        }

        public static StubRouteRuleBuilder When(Predicate<HttpContext> condition)
        {
            return StubRouteRuleBuilder.When(condition);
        }

        public StubRouteRule WithStatusCode(int statusCode)
        {
            StatusCode = statusCode;
            return this;
        }

        public bool Matches(HttpContext context)
        {
            return Condition.Invoke(context);
        }

        public async Task Apply(HttpContext context)
        {
            var content = await ResponseContent.Content;

            var buffer = Encoding.UTF8.GetBytes(content);

            context.Response.StatusCode = this.StatusCode;

            await context.Response.Body.WriteAsync(buffer);
        }
    }
}