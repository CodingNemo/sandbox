﻿using System.Text.Json;

namespace Dependency.mock.Extensions.Content;

public class StubResponseFromObject : IStubResponseContent
{
    private readonly object _obj;

    private string? _content;

    public StubResponseFromObject(object obj)
    {
        _obj = obj;
    }

    public Task<string> Content => LoadContent();

    private async Task<string> LoadContent()
    {
        if (_content is not null)
        {
            return _content;
        }

        await using var memory = new MemoryStream();
        await JsonSerializer.SerializeAsync(memory, _obj);

        memory.Seek(0, SeekOrigin.Begin);

        using var reader = new StreamReader(memory);

        return await reader.ReadToEndAsync();

    }
}