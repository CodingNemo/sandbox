﻿namespace Dependency.mock.Extensions.Content;

public interface IStubResponseContent
{
    Task<string> Content { get; } 
}