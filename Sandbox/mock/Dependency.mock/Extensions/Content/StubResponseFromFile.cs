﻿namespace Dependency.mock.Extensions.Content;

public class StubResponseFromFile : IStubResponseContent
{
    private readonly string _filePath;

    public StubResponseFromFile(string filePath)
    {
        _filePath = filePath;
    }

    public Task<string> Content => File.ReadAllTextAsync(_filePath);
}