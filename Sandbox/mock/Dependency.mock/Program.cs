var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.StubGet("/analyst",
    route => route.AlwaysReturnFileContent("./Mocks/GetAnalystResponse.json")
);


app.StubPost("/analyst",
    route => route.AlwaysReturnContent(new
    {
        Error = "You cannot create a new analyst"
    }, StatusCodes.Status405MethodNotAllowed)
);



// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();
app.Logger.LogInformation("RUNNING MOCK");
app.MapControllers();

app.Run();
