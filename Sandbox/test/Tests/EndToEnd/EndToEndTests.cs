using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using Ductus.FluentDocker.Builders;
using Ductus.FluentDocker.Model.Common;
using NFluent;
using Tests.EndToEnd.Extensions;
using Xunit;

namespace Tests.EndToEnd;

public class EndToEndTests
{
    [Fact]
    public async Task Average()
    {
        var currentDirectory =  Directory.GetParent(Assembly.GetExecutingAssembly().Location);
        var dockerComposeFile = Path.Combine(currentDirectory!.FullName,
            (TemplateString)"../../../../../docker-compose.yml");

        using var svc = new Builder()
            .UseContainer()
            .UseCompose()
            .FromFile(dockerComposeFile)
            .RemoveOrphans()
            .ForceRecreate()
            .ForceBuild()
            .WaitForHttp("consumer", "http://localhost:5100/swagger/index.html")
            .Build().Start();

        using var httpClient = new HttpClient();

        var averageResponse = await httpClient.GetAsync("http://localhost:5100/api/TemperatureStats/average");

        Check.That(averageResponse.IsSuccessStatusCode).IsTrue();

        var result = await averageResponse.Content.ReadFromJsonAsync<JsonElement>();

        var averageProperty = result.GetProperty("average");
        var average = averageProperty.GetNullableDouble();
        
        Check.That(average).HasAValue();

        var analystProperty = result.GetProperty("analyst");
        var analyst = analystProperty.GetString();

        Check.That(analyst).IsNotNull().And.IsNotEqualTo("");
    }

    [Fact]
    public async Task Average_WithMock()
    {
        var currentDirectory = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
        var dockerComposeFile = Path.Combine(currentDirectory!.FullName,
            (TemplateString)"../../../../../docker-compose.yml");

        var dockerComposeWithMockFile = Path.Combine(currentDirectory!.FullName,
            (TemplateString)"../../../../../docker-compose.mock.yml");

        using var svc = new Builder()
            .UseContainer()
            .UseCompose()
            .FromFile(dockerComposeFile, dockerComposeWithMockFile)
            .RemoveOrphans()
            .ForceRecreate()
            .ForceBuild()
            .WaitForHttp("consumer", "http://localhost:5100/swagger/index.html")
            .Build().Start();

        using var httpClient = new HttpClient();

        var averageResponse = await httpClient.GetAsync("http://localhost:5100/api/TemperatureStats/average");

        Check.That(averageResponse.IsSuccessStatusCode).IsTrue();

        var result = await averageResponse.Content.ReadFromJsonAsync<JsonElement>();

        var averageProperty = result.GetProperty("average");
        var average = averageProperty.GetNullableDouble();

        Check.That(average).HasAValue();

        var analystProperty = result.GetProperty("analyst");
        var analyst = analystProperty.GetString();

        Check.That(analyst).IsNotNull().And.IsEqualTo("Mock MOCK");
    }
}