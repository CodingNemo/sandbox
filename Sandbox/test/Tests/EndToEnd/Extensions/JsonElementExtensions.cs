﻿using System.Text.Json;

namespace Tests.EndToEnd.Extensions;

public static class JsonElementExtensions
{
    public static double? GetNullableDouble(this JsonElement element)
    {
        if (!element.TryGetDouble(out var average))
        {
            return null;
        }

        return average;
    }
}