﻿using System.Collections.Generic;
using System.Linq;
using NFluent;

namespace ArchUnitNET.Fluent;

public static class EvaluationResultExtensions
{
    public static ICheckLink<ICheck<IEnumerable<EvaluationResult>>> Passed(
        this ICheck<IEnumerable<EvaluationResult>> check)
    {
        var checker = NFluent.Extensibility.ExtensibilityHelper.ExtractChecker(check);

        return checker.ExecuteCheck(() =>
        {
            var evaluation = checker.Value;

            var failed = evaluation.Where(res => !res.Passed);
            Check.That(failed.Select(r => $"{r.EvaluatedObjectIdentifier} does not match rule : {r.ArchRule.Description}")).IsEmpty();
        }, "");

    }
}