﻿namespace Tests.Architecture.Extensions;

public abstract record NamingRule(string? AdditionalReason)
{
    public abstract NamingRule WithAdditionalReason(string additionalReason);

    private record AlternativeNameRule(string Name) : NamingRule((string?) null)
    {
        public override NamingRule WithAdditionalReason(string additionalReason)
        {
            return new AlternativeNameRule(Name)
            {
                AdditionalReason = additionalReason
            };
        }

        public override string ToString() => $@"it is not a valid domain name. Use ""{Name}"" instead. {AdditionalReason}";
    };

    public static NamingRule Alternative(string name) => new AlternativeNameRule(name);

    private record ForbiddenNameRule() : NamingRule((string?)null)
    {
        public override NamingRule WithAdditionalReason(string additionalReason)
        {
            return new ForbiddenNameRule()
            {
                AdditionalReason = additionalReason
            };
        }

        public override string ToString() => $"it is forbidden. Don't use it. {AdditionalReason}";
    };

    public static readonly NamingRule Forbidden = new ForbiddenNameRule();
}