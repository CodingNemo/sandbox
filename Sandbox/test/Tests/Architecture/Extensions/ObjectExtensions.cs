﻿using System.Collections.Generic;
using System.Linq;

namespace Tests.Architecture.Extensions;

internal static class ObjectExtensions
{
    public static IEnumerable<object[]> ToAlternativeMapping(this object o)
    {
        var properties = o.GetType().GetProperties();

        return properties.Select(p =>
        {
            var value = p.GetValue(o) ?? NamingRule.Forbidden;

            return value switch
            {
                NamingRule alternative => new[] {p.Name, value},
                _ => new object [] {p.Name, NamingRule.Alternative(value.ToString()!)}

            };
        });
    }
}