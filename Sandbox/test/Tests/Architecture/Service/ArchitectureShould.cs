﻿using System.Collections.Generic;
using ArchUnitNET.Domain;
using ArchUnitNET.Fluent;
using ArchUnitNET.Loader;
using NFluent;
using Service.DOMAIN;
using Tests.Architecture.Extensions;
using Xunit;
using static ArchUnitNET.Fluent.ArchRuleDefinition;

namespace Tests.Architecture.Service;

public class ArchitectureShould
{
    private static readonly ArchUnitNET.Domain.Architecture Architecture =
        new ArchLoader().LoadAssemblies(typeof(WeatherForecasts).Assembly).Build();

    private readonly IObjectProvider<IType> Domain =
        Types().That().ResideInNamespace("Service.DOMAIN").As("Domain");

    private readonly IObjectProvider<IType> Api =
        Types().That().ResideInNamespace("Service.API").As("Api");

    private readonly IObjectProvider<IType> Spi =
        Types().That().ResideInNamespace("Service.SPI").As("Spi");


    [Fact]
    public void Ensure_domain_does_not_depend_on_spi()
    {
        IArchRule domainShouldNotDependOnSpiLayerRule = Types().That()
            .Are(Domain).Should()
            .NotDependOnAny(Spi)
            .Because("Domain layer should be isolated from spi");

        var evaluation = domainShouldNotDependOnSpiLayerRule.Evaluate(Architecture);

        Check.That(evaluation).Passed();
    }

    [Fact]
    public void Ensure_domain_does_not_depend_on_api()
    {
        IArchRule domainShouldNotDependOnApiLayerRule = Types().That()
            .Are(Domain).Should().NotDependOnAny(Api)
            .Because("Domain layer should be isolated from api");

        var evaluation = domainShouldNotDependOnApiLayerRule.Evaluate(Architecture);
        Check.That(evaluation).Passed();
    }

    private static IEnumerable<object[]> ForbiddenNames() =>
        new
        {
            Dto = NamingRule.Forbidden,
            //Invalid = NamingRule.Forbidden,
            //Divination = NamingRule.Alternative("Forecast").WithAdditionalReason("We chose Forecast because it was more appropriate and less ambiguous."),
        }.ToAlternativeMapping();

    [Theory]
    [MemberData(nameof(ForbiddenNames))]
    public void Ensure_domain_does_not_have_names_containing_forbidden_name(string forbiddenName, NamingRule namingRule)
    {
        IArchRule domainShouldNotHaveTypeWithNameContainingForbiddenNameRule = Types().That()
            .Are(Domain).Should().NotHaveNameContaining(forbiddenName)
            .Because($"{namingRule}");

        IArchRule domainShouldNotHaveTypeWithMembersWhichNameContainsForbiddenNameRule = Members().That().AreDeclaredIn(
                Types().That()
                    .Are(Domain))
            .Should().NotHaveNameContaining(forbiddenName)
            .Because($"{namingRule}");

        var domainNamingRules =
            domainShouldNotHaveTypeWithNameContainingForbiddenNameRule.And(
                domainShouldNotHaveTypeWithMembersWhichNameContainsForbiddenNameRule);

        var evaluation = domainNamingRules.Evaluate(Architecture);

        Check.That(evaluation).Passed();
    }
}
