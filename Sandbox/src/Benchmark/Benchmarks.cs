﻿
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

public struct TotoStruct
{
    public string Property { get; set; }
}

public class TotoClass
{
    public string? Property { get; set; }
}

[MemoryDiagnoser(false)]
public class Benchmarks
{

    [Benchmark]
    public List<TotoStruct> ListOfStruct()
    {
        return new List<TotoStruct>()
        {
            new TotoStruct { Property = "Toto" }
        };
    }
    
    [Benchmark]
    public TotoStruct[] ArrayOfStruct()
    {
        return new TotoStruct[]
        {
            new TotoStruct { Property = "Toto" }
        };
    }
    
    [Benchmark]
    public List<TotoClass> ListOfClass()
    {
        return new List<TotoClass>()
        {
            new TotoClass { Property = "Toto" }
        };
    }
    
    [Benchmark]
    public TotoClass[] ArrayOfClass()
    {
        return new TotoClass[]
        {
            new TotoClass { Property = "Toto" }
        };
    }
    
    [Benchmark]
    public TotoStruct ListOfStructAsParameter()
    {
        return DoSomethingToTotoStructsList(new List<TotoStruct>()
        {
            new TotoStruct { Property = "Toto" }
        });
    }

    private static TotoStruct DoSomethingToTotoStructsList(IEnumerable<TotoStruct> doSomethingToTotoStructs)
    {
        return doSomethingToTotoStructs.FirstOrDefault();
    }

    [Benchmark]
    public TotoStruct ArrayOfStructAsParameter()
    {
        return DoSomethingToTotoStructs(new TotoStruct[]
        {
            new TotoStruct { Property = "Toto" }
        });
    }

    private static TotoStruct DoSomethingToTotoStructs(IEnumerable<TotoStruct> doSomethingToTotoStructs)
    {
        return doSomethingToTotoStructs.FirstOrDefault();
    }

    [Benchmark]
    public TotoClass? ListOfClassAsParameter()
    {
        return TotoClasses(new List<TotoClass>()
        {
            new TotoClass { Property = "Toto" }
        });
    }

    private static TotoClass? TotoClasses(IEnumerable<TotoClass> totoClasses)
    {
        return totoClasses.FirstOrDefault();
    }

    [Benchmark]
    public TotoClass? ArrayOfClassAsParameters()
    {
        return DoSomethingToArrayClasses(new TotoClass[]
        {
            new TotoClass { Property = "Toto" }
        });
    }

    private static TotoClass? DoSomethingToArrayClasses(IEnumerable<TotoClass> doSomethingToArrayClasses)
    {
        return doSomethingToArrayClasses.FirstOrDefault();
    }
}