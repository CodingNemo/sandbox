namespace Consumer.HttpClients;

public record WeatherForecasts(IEnumerable<WeatherForecastByDate> DailyForecasts, int NbOfDays, string Analyst)
{
    public double Average => DailyForecasts.Average(f => f.TemperatureC);
};

public record WeatherForecastByDate(DateTime Date, int TemperatureC, int TemperatureF, string? Summary);
