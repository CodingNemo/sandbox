﻿using Core;
using Core.HttpClient;
using HttpRequest = Core.HttpClient.HttpRequest;

namespace Consumer.HttpClients;

public class WeatherForecastHttpClient
{
    private readonly HttpClient httpClient;

    public WeatherForecastHttpClient(HttpClient httpClient)
    {
        this.httpClient = httpClient;
    }

    public Task<Result<WeatherForecasts, HttpCallErrors>> GetForecasts()
    {
        var request = HttpRequest.Get("/WeatherForecast");

        return 
            from httpCallResponse in httpClient.Run<WeatherForecasts>(request)
            select httpCallResponse.Content;
    }
}