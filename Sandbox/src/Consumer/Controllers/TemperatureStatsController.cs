﻿using Consumer.HttpClients;
using Core;
using Microsoft.AspNetCore.Mvc;

namespace Consumer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TemperatureStatsController : ControllerBase
{
    private readonly WeatherForecastHttpClient _httpClient;

    public TemperatureStatsController(WeatherForecastHttpClient httpClient)
    {
        this._httpClient = httpClient;
    }

    [HttpGet]
    [Route("average")]
    public async Task<ActionResult<AverageTemperatureForecast>> GetAverage() =>
        from forecasts in await _httpClient.GetForecasts()
        select new AverageTemperatureForecast(forecasts.Average, forecasts.Analyst);
}

public record AverageTemperatureForecast(double Average, string Analyst);