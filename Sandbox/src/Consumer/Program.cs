using Consumer;
using Consumer.HttpClients;
using Microsoft.Extensions.Options;
using Polly;


var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var waitAndRetryPolicy = Policy
    .Handle<Exception>()
    .OrResult<HttpResponseMessage>(response => !response.IsSuccessStatusCode)
    .WaitAndRetryAsync(new[]
    {
        TimeSpan.FromMilliseconds(100),
        TimeSpan.FromMilliseconds(500),
        TimeSpan.FromSeconds(1),
        TimeSpan.FromSeconds(2),
        TimeSpan.FromSeconds(3),
    });

builder.Services.AddHttpClient<WeatherForecastHttpClient>((services, httpClient) =>
{
    var options = services.GetRequiredService<IOptions<ServiceConfigOptions>>();
    httpClient.BaseAddress = options.Value.Url;
}).AddPolicyHandler(waitAndRetryPolicy);

builder.Services.AddOptions<ServiceConfigOptions>()
    .ValidateDataAnnotations()
    .Bind(builder.Configuration.GetSection(ServiceConfigOptions.SectionName));

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.MapControllers();

app.Run();

