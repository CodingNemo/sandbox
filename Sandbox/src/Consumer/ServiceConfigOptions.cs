﻿using System.ComponentModel.DataAnnotations;

namespace Consumer;

public class ServiceConfigOptions
{
    public const string SectionName = "Service";

    [Required]
    public Uri? Url { get; set; }
};