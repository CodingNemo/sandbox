using Microsoft.AspNetCore.Mvc;

namespace Dependency.Controllers;

[ApiController]
[Route("[controller]")]
public class AnalystController : ControllerBase
{
    private static readonly string[] analysts = new[]
    {
        "Beno�t", "Ahmed", "Yi"
    };

    
    [HttpGet]
    public ActionResult<Analyst> Index()
    {
        var dice = Random.Shared.Next(0, 1000);

        if (dice % 7 == 0)
        {
            return NotFound();
        }

        var analyst = PickAnalyst();

        if (dice % 11 == 0)
        {
            return Conflict(new
            {
                Message = "Analyst was too lazy to come to work today",
                Analyst = analyst
            });
        }

        return Ok(analyst);
    }

    private static Analyst PickAnalyst()
    {
        var analystNameIndex = Random.Shared.Next(analysts.Length);
        var analystName = analysts[analystNameIndex];
        return new Analyst(analystName);
    }

    public record Analyst(string Name);
}
