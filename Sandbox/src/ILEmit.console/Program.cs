﻿using ILEmit.console;
using System.Reflection;

var ctorParams = new object[2];

Console.Write("Enter a integer value for X: ");
var myX = Console.ReadLine();
Console.Write("Enter a integer value for Y: ");
var myY = Console.ReadLine();

Console.WriteLine("---");

ctorParams[0] = Convert.ToInt32(myX);
ctorParams[1] = Convert.ToInt32(myY);

var ptType = EmitWriteLineDemo.CreateDynamicType();

if (ptType is null)
    throw new Exception($"{nameof(ptType)} is null");

var ptInstance = Activator.CreateInstance(ptType, ctorParams);

ptType.InvokeMember("WritePoint",
        BindingFlags.InvokeMethod,
        null,
        ptInstance,
        Array.Empty<object>());
