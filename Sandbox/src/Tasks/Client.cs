﻿namespace Tasks;

public class Client
{
    private readonly IOService ioService;

    public Client()
    {
        ioService = new IOService();
    }
    
    static Task<int> GetId(IOService ioService)
    {
        return ioService.GetId();
    }

    static async Task<int> GetIdAsync(IOService ioService)
    {
        return await ioService.GetId();
    }

    public async Task DisplayIds()
    {
        var id1 = await GetId(ioService);
        Console.WriteLine($"Id1: {id1}");


        var id2 = await GetIdAsync(ioService);
        Console.WriteLine($"Id2: {id2}");
    }
}