﻿namespace Tasks;

public class IOService
{
    public async Task<int> GetId()
    {
        await Task.Delay(TimeSpan.FromSeconds(2));
        return Random.Shared.Next();
    }
}