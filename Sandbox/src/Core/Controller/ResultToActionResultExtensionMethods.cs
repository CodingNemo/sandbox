﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Core.Controller;

public static class ResultToActionResultExtensionMethods
{
    public static ActionResultBuilder<TSuccess, TError>
        ToActionResult<TSuccess, TError>(this Result<TSuccess, TError> result) where TError : Error =>
        new(result);

    public class ActionResultBuilder<TSuccess, TError>
        where TError : Error
    {
        public class MissingActionResultMapping<TOrigin> : Exception
        {
            public TOrigin OriginData { get; }

            public MissingActionResultMapping(TOrigin data)
            {
                OriginData = data;
            }
        }

        public delegate ActionResult<TSuccess> SuccessHandler(TSuccess data);
        public delegate bool IsSuccessPredicate(TSuccess data);

        public delegate ActionResult<TSuccess> ErrorHandler<in TSpecificError>(TSpecificError data) where TSpecificError : Error;
        public delegate ActionResult<TSuccess> ErrorHandler(TError data);
        public delegate bool IsErrorPredicate(TError data);

        private readonly Result<TSuccess, TError> _result;


        private readonly Stack<(IsErrorPredicate whenError, ErrorHandler onError)> _onErrors = new();

        private readonly Stack<(IsSuccessPredicate whenSuccess, SuccessHandler onError)> _onSuccess = new();


        internal ActionResultBuilder(Result<TSuccess, TError> result)
        {
            _result = result;
            _onErrors.Push((_ => true, _ => new StatusCodeResult(StatusCodes.Status500InternalServerError)));
            _onSuccess.Push((_ => true, data => new OkObjectResult(data)));
        }

        public ActionResult<TSuccess> Build()
        {
            return _result switch
            {
                Result<TSuccess, TError>.SuccessResult success => ApplySuccessHandler(success.Data),
                Result<TSuccess, TError>.ErrorResult error => ApplyErrorHandler(error.ErrorInfo),
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        private ActionResult<TSuccess> ApplySuccessHandler(TSuccess data)
        {
            foreach (var (predicate, handler) in _onSuccess)
            {
                if (predicate(data))
                {
                    return handler(data);
                }
            }

            throw new MissingActionResultMapping<TSuccess>(data);
        }

        private ActionResult<TSuccess> ApplyErrorHandler(TError error)
        {
            foreach (var (predicate, handler) in _onErrors)
            {
                if (predicate(error))
                {
                    return handler(error);
                }
            }

            throw new MissingActionResultMapping<TError>(error);
        }


        public ActionResultBuilder<TSuccess, TError> OnSuccess(SuccessHandler onSuccess)
        {
            return OnSuccess(x => true, onSuccess);
        }

        public ActionResultBuilder<TSuccess, TError> OnSuccess(IsSuccessPredicate whenSuccess, SuccessHandler onSuccess)
        {
            _onSuccess.Push((whenSuccess, onSuccess));
            return this;
        }


        public ActionResultBuilder<TSuccess, TError> OnError<TSpecificError>(ErrorHandler<TSpecificError> onError) where TSpecificError : TError
        {
            ActionResult<TSuccess> Callback(TError err) => onError((TSpecificError)err);
            bool WhenError(TError error) => error is TSpecificError;
            _onErrors.Push((WhenError, Callback));
            return this;
        }

        public ActionResultBuilder<TSuccess, TError> OnError(IsErrorPredicate whenError, ErrorHandler onError)
        {
            _onErrors.Push((whenError, onError));
            return this;
        }

        public static implicit operator ActionResult<TSuccess>(ActionResultBuilder<TSuccess, TError> builder) =>
            builder.Build();
    }
}


