﻿using Core.Controller;
using Microsoft.AspNetCore.Mvc;

namespace Core;

public partial record Result<TSuccess, TError> where TError : Error
{
    public static implicit operator ActionResult<TSuccess>(Result<TSuccess, TError> result) => result.ToActionResult();
}

