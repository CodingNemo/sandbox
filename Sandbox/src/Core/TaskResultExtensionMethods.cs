﻿
namespace Core
{
    public static class TaskResultExtensionMethods
    {
        public static async Task<Result<TResultSuccess, TError>> Select<TSourceSuccess, TResultSuccess,
            TError>(
            this Task<Result<TSourceSuccess, TError>> sourceTask,
            Func<TSourceSuccess, TResultSuccess> selector)
            where TError : Error
        {
            var source = await sourceTask;
            return source.Select(selector);
        }


        public static async Task<Result<TResultSuccess, TError>> Select<TSourceSuccess, TResultSuccess,
            TError>(
            this Task<Result<TSourceSuccess, TError>> sourceTask,
            Func<TSourceSuccess, Task<TResultSuccess>> selector)
            where TError : Error
        {
            var source = await sourceTask;
            return await source.Select(selector);
        }

        public static async Task<Result<TResultSuccess, TError>> SelectMany<TSourceSuccess, TResultSuccess,
            TError>(
            this Task<Result<TSourceSuccess, TError>> sourceTask,
            Func<TSourceSuccess, Result<TResultSuccess, TError>> selector)
        where TError : Error
        {
            var source = await sourceTask;
            return source.SelectMany(selector);
        }

        public static Task<Result<TResultSuccess, TError>> SelectMany<TSourceSuccess, TResultSuccess,
            TError>(
            this Task<Result<TSourceSuccess, TError>> sourceTask,
            Func<TSourceSuccess, Task<Result<TResultSuccess, TError>>> selector)
            where TError : Error
        {
            return sourceTask.SelectMany(selector, (_, intermediate) => intermediate);
        }


        public static async Task<Result<TSuccessOut, TError>> SelectMany<TSuccessIn, TSuccessIntermediate, TSuccessOut,
            TError>(
            this Task<Result<TSuccessIn, TError>> sourceTask,
            Func<TSuccessIn, Result<TSuccessIntermediate, TError>> selectManySelector,
            Func<TSuccessIn, TSuccessIntermediate, TSuccessOut> resultSelector)
            where TError : Error
        {
            var source = await sourceTask;
            return source.SelectMany(selectManySelector, resultSelector);
        }

        public static async Task<Result<TSuccessOut, TError>> SelectMany<TSuccessIn, TSuccessIntermediate, TSuccessOut,
            TError>(
            this Task<Result<TSuccessIn, TError>> sourceTask,
            Func<TSuccessIn, Task<Result<TSuccessIntermediate, TError>>> selectManySelector,
            Func<TSuccessIn, TSuccessIntermediate, TSuccessOut> resultSelector)
            where TError : Error
        {
            var source = await sourceTask;
            return await source.SelectMany(selectManySelector, resultSelector);
        }

        public static async Task<Result<TSuccessOut, TError>> SelectMany<TSuccessIn, TSuccessIntermediate, TSuccessOut,
            TError>(
            this Task<Result<TSuccessIn, TError>> sourceTask,
            Func<TSuccessIn, Task<Result<TSuccessIntermediate, TError>>> selectManySelector,
            Func<TSuccessIn, TSuccessIntermediate, Task<TSuccessOut>> resultSelector)
            where TError : Error
        {
            var source = await sourceTask;
            return await source.SelectMany(selectManySelector, resultSelector);
        }

    }
}
