﻿using System.Net;
using System.Net.Http.Json;
using System.Runtime.Serialization;
using System.Text.Json;
using Microsoft.AspNetCore.Http.Extensions;

namespace Core.HttpClient;

public record HttpCallSuccess<TData>(HttpStatusCode StatusCode, TData Content);

public abstract record HttpCallErrors : Error
{
    public record HttpErrorsStatusErrors(HttpStatusCode StatusCode, HttpContent ErrorContent) : HttpCallErrors;
    public static HttpCallErrors HttpErrorStatus(HttpStatusCode statusCode, HttpContent errorContent) =>
        new HttpErrorsStatusErrors(statusCode, errorContent);


    public record TimeoutErrors : HttpCallErrors;
    public static HttpCallErrors Timeout() => new TimeoutErrors();


    public record NetworkErrors(string Message) : HttpCallErrors;
    public static HttpCallErrors Network(string message) => new NetworkErrors(message);


    public record DeserializationErrors(Type TargetType, HttpContent Content, string Message) : HttpCallErrors;
    public static HttpCallErrors Deserialization(Type targetType, HttpContent content, string message) =>
        new DeserializationErrors(targetType, content, message);
}

public class UrlBuilder
{
    private readonly string _baseUri;

    private readonly QueryBuilder _queryBuilder = new();

    public UrlBuilder(string baseUri)
    {
        _baseUri = baseUri;
    }

    public UrlBuilder AddQuery<T>(string name, T value, Predicate<T>? condition = null)
    {
        if (condition?.Invoke(value) ?? true)
        {
            _queryBuilder.Add(name, $"{value}");
        }

        return this;
    }

    public Uri Build()
    {
        var kind = _baseUri.StartsWith("http") || _baseUri.StartsWith("https") ? UriKind.Absolute : UriKind.Relative;
        return new Uri($"{_baseUri}?{_queryBuilder.ToQueryString()}", kind);
    }
}

public abstract record HttpRequest(HttpMethod Method, string _baseUrl, Action<UrlBuilder>? _buildUrl = null)
{
    private readonly Action<UrlBuilder>? _buildUrl = _buildUrl;
    private readonly string _baseUrl = _baseUrl;

    public record RequestWithoutPayload(HttpMethod Method, string _baseUrl, Action<UrlBuilder>? _buildUrl = null) : HttpRequest(Method, _baseUrl, _buildUrl);

    public record RequestWithPayload(HttpMethod Method, string _baseUrl, Action<UrlBuilder>? _buildUrl = null) : HttpRequest(Method, _baseUrl, _buildUrl)
    {
        private HttpContent? _payload;

        public HttpRequest WithJsonPayload<T>(T obj)
        {
            _payload = JsonContent.Create(obj);
            return this;
        }


        public override HttpRequestMessage BuildMessage()
        {
            var baseMessage = base.BuildMessage();
            baseMessage.Content = _payload;
            return baseMessage;
        }
    };

    public static HttpRequest Get(string uri, Action<UrlBuilder>? buildUrl = null) => new RequestWithoutPayload(HttpMethod.Get, uri, buildUrl);
    public static HttpRequest Delete(string uri, Action<UrlBuilder>? buildUrl = null) => new RequestWithoutPayload(HttpMethod.Delete, uri, buildUrl);
    public static RequestWithPayload Post(string uri, Action<UrlBuilder>? buildUrl = null) => new(HttpMethod.Post, uri, buildUrl);
    public static RequestWithPayload Put(string uri, Action<UrlBuilder>? buildUrl = null) => new(HttpMethod.Put, uri, buildUrl);

    public virtual HttpRequestMessage BuildMessage()
    {
        var urlBuilder = new UrlBuilder(_baseUrl);

        _buildUrl?.Invoke(urlBuilder);

        var uri = urlBuilder.Build();

        return new HttpRequestMessage(Method, uri);
    }
}

public static class HttpClientResultExtensions
{
    public static async Task<Result<HttpCallSuccess<TData>, HttpCallErrors>> Run<TData>(this System.Net.Http.HttpClient httpClient, HttpRequest request)
    {
        try
        {
            var message = request.BuildMessage();

            var response = await httpClient.SendAsync(message);

            if (!response.IsSuccessStatusCode)
            {
                return HttpCallErrors.HttpErrorStatus(response.StatusCode, response.Content);
            }

            return await (
                from data in Deserialize<TData>(response)
                select new HttpCallSuccess<TData>(response.StatusCode, data));
        }
        catch (TimeoutException)
        {
            return HttpCallErrors.Timeout();
        }
        catch (HttpRequestException hre)
        {
            return HttpCallErrors.Network(hre.Message);
        }
    }

    private static async Task<Result<TData?, HttpCallErrors>> Deserialize<TData>(HttpResponseMessage message)
    {
        try
        {
            var content = await message.Content.ReadFromJsonAsync<TData>();
            return content;

        }
        catch (JsonException e)
        {
            return HttpCallErrors.Deserialization(typeof(TData), message.Content, e.Message);
        }
    }
}


[Serializable]
public class UnsupportedStatusCodeException : Exception
{
    public HttpStatusCode? StatusCode { get; }
    public HttpRequest? Request { get; }

    public UnsupportedStatusCodeException(HttpStatusCode statusCode, HttpRequest request) : base($"Unsupported response status code {statusCode} when sending request {request}")
    {
        StatusCode = statusCode;
        Request = request;
    }

    protected UnsupportedStatusCodeException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}

