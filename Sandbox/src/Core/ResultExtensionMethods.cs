﻿namespace Core
{
    public static class ResultExtensionMethods
    {
        public static Result<TResultSuccess, TError> Select<TSourceSuccess, TResultSuccess,
            TError>(
            this Result<TSourceSuccess, TError> source,
            Func<TSourceSuccess, TResultSuccess> selector)
            where TError : Error
        {
            return source switch
            {
                Result<TSourceSuccess, TError>.SuccessResult success => Result<TResultSuccess, TError>.Success(selector(success.Data)),
                Result<TSourceSuccess, TError>.ErrorResult error =>
                    Result<TResultSuccess, TError>.Error(error.ErrorInfo),
                _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
            };
        }

        public static Result<TSuccess, TErrorOut> SelectError<TSuccess, TErrorIn,
            TErrorOut>(
            this Result<TSuccess, TErrorIn> source,
            Func<TErrorIn, TErrorOut> selector)
            where TErrorIn : Error
            where TErrorOut : Error
        {
            return source switch
            {
                Result<TSuccess, TErrorIn>.SuccessResult success => Result<TSuccess, TErrorOut>.Success(success.Data),
                Result<TSuccess, TErrorIn>.ErrorResult error =>
                    Result<TSuccess, TErrorOut>.Error(selector(error.ErrorInfo)),
                _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
            };
        }
        public static async Task<Result<TSuccess, TErrorOut>> SelectError<TSuccess, TErrorIn,
            TErrorOut>(
            this Result<TSuccess, TErrorIn> source,
            Func<TErrorIn, Task<TErrorOut>> selector)
            where TErrorIn : Error
            where TErrorOut : Error
        {
            return source switch
            {
                Result<TSuccess, TErrorIn>.SuccessResult success => Result<TSuccess, TErrorOut>.Success(success.Data),
                Result<TSuccess, TErrorIn>.ErrorResult error =>
                    Result<TSuccess, TErrorOut>.Error(await selector(error.ErrorInfo)),
                _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
            };
        }

        public static async Task<Result<TSuccess, TErrorOut>> SelectError<TSuccess, TErrorIn,
            TErrorOut>(
            this Task<Result<TSuccess, TErrorIn>> tSource,
            Func<TErrorIn, TErrorOut> selector)
            where TErrorIn : Error
            where TErrorOut : Error
        {
            var source = await tSource;
            return source.SelectError(selector);
        }

        public static async Task<Result<TResultSuccess, TError>> Select<TSourceSuccess, TResultSuccess,
            TError>(
            this Result<TSourceSuccess, TError> source,
            Func<TSourceSuccess, Task<TResultSuccess>> selector)
            where TError : Error
        {
            return source switch
            {
                Result<TSourceSuccess, TError>.SuccessResult success =>
                    Result<TResultSuccess, TError>.Success(await selector(success.Data)),
                Result<TSourceSuccess, TError>.ErrorResult error =>
                    Result<TResultSuccess, TError>.Error(error.ErrorInfo),
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public static Result<TResultSuccess, TError> SelectMany<TSourceSuccess, TResultSuccess,
            TError>(
            this Result<TSourceSuccess, TError> source,
            Func<TSourceSuccess, Result<TResultSuccess, TError>> selector)
        where TError : Error
        {
            return source.SelectMany(selector, (_, intermediate) => intermediate);
        }

        public static Result<TSuccessOut, TError> SelectMany<TSuccessIn, TSuccessIntermediate, TSuccessOut,
            TError>(
            this Result<TSuccessIn, TError> source,
            Func<TSuccessIn, Result<TSuccessIntermediate, TError>> selectManySelector,
            Func<TSuccessIn, TSuccessIntermediate, TSuccessOut> resultSelector)
            where TError : Error
        {
            return source switch
            {
                Result<TSuccessIn, TError>.SuccessResult success => selectManySelector(success.Data).Select(intermediate => resultSelector(success.Data, intermediate)),
                Result<TSuccessIn, TError>.ErrorResult error =>
                    Result<TSuccessOut, TError>.Error(error.ErrorInfo),
                _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
            };
        }

        public static async Task<Result<TSuccessOut, TError>> SelectMany<TSuccessIn, TSuccessIntermediate, TSuccessOut,
            TError>(
            this Result<TSuccessIn, TError> source,
            Func<TSuccessIn, Task<Result<TSuccessIntermediate, TError>>> selectManySelector,
            Func<TSuccessIn, TSuccessIntermediate, TSuccessOut> resultSelector)
            where TError : Error
        {
            return source switch
            {
                Result<TSuccessIn, TError>.SuccessResult success => (await selectManySelector(success.Data))
                    .Select(intermediate => resultSelector(success.Data, intermediate)),
                Result<TSuccessIn, TError>.ErrorResult error =>
                    Result<TSuccessOut, TError>.Error(error.ErrorInfo),
                _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
            };
        }

        public static async Task<Result<TSuccessOut, TError>> SelectMany<TSuccessIn, TSuccessIntermediate, TSuccessOut,
            TError>(
            this Result<TSuccessIn, TError> source,
            Func<TSuccessIn, Task<Result<TSuccessIntermediate, TError>>> selectManySelector,
            Func<TSuccessIn, TSuccessIntermediate, Task<TSuccessOut>> resultSelector)
            where TError : Error
        {
            return source switch
            {
                Result<TSuccessIn, TError>.SuccessResult success => await (await selectManySelector(success.Data))
                    .Select(intermediate => resultSelector(success.Data, intermediate)),
                Result<TSuccessIn, TError>.ErrorResult error =>
                    Result<TSuccessOut, TError>.Error(error.ErrorInfo),
                _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
            };
        }

    }
}
