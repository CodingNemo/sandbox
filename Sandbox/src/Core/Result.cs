﻿namespace Core;


public abstract record Error;

public  abstract partial record Result<TSuccess, TError> 
    where TError: Error
{
    public record SuccessResult(TSuccess Data) : Result<TSuccess, TError>;

    public record ErrorResult(TError ErrorInfo) : Result<TSuccess, TError>;

    public static Result<TSuccess, TError> Success(TSuccess data) => new SuccessResult(data);
    public static Result<TSuccess, TError> Error(TError error) => new ErrorResult(error);

    public static implicit operator Result<TSuccess, TError>(TSuccess success) => new SuccessResult(success);
    public static implicit operator Result<TSuccess, TError>(TError error) => new ErrorResult(error);
}
