using System.Text.Json.Serialization;
using Microsoft.Extensions.Options;
using Service.API;
using Service.SPI;

var builder = WebApplication.CreateBuilder(args);

// AddQuery services to the container.
builder.Services.RegisterWeatherServices();

builder.Services.AddControllers().AddJsonOptions(
    jsonOpts =>
    {
        jsonOpts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddOptions<DependencyOptions>()
    .ValidateDataAnnotations()
    .Bind(builder.Configuration.GetSection(DependencyOptions.SectionName));


builder.Services.AddHttpClient<AnalystHttpClient>((provider, httpClient) =>
{
    var options = provider.GetRequiredService<IOptions<DependencyOptions>>();
    httpClient.BaseAddress = options.Value.Url;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.MapControllers();

app.Run();