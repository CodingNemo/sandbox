
using System.ComponentModel.DataAnnotations;

public record DependencyOptions
{
    public const string SectionName = "Dependency";

    [Required]
    public Uri? Url { get; set; }
}