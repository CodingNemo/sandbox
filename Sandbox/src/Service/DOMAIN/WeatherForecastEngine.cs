﻿using Core;

namespace Service.DOMAIN;

public class WeatherForecastEngine
{
    private readonly IRequireSomeAnalyst _analystProvider;
    private static readonly ForecastSummary[] Summaries = Enum.GetValues<ForecastSummary>();

    private const int DaysLimit = 7;

    public WeatherForecastEngine(IRequireSomeAnalyst analystProvider)
    {
        _analystProvider = analystProvider;
    }

    public Task<Result<WeatherForecasts, ForecastsErrors>> GetForecasts(int nbDays, DateTimeOffset startDate) => 
    
        from analyst in _analystProvider.GetFirstFreeAnalyst().SelectError(ForecastsErrors.AnalystError)
        from forecast in GetWeatherForecast(nbDays, startDate, analyst)
        select forecast;


    private Result<WeatherForecasts, ForecastsErrors> GetWeatherForecast(int nbDays, DateTimeOffset startDate,
        Analyst analyst)
    {
        var dice = Random.Shared.Next(0, 1000);

        if (dice % 7 == 0)
        {
            return ForecastsErrors.MissingForecasts();
        }

        if (nbDays > DaysLimit)
        {
            return ForecastsErrors.TooManyDays(nbDays, DaysLimit);
        }

        var forecasts = Enumerable.Range(1, nbDays).Select(index =>
            {
                var summary = GenerateSummary();
                var temperatureC = GenerateTemperature(summary);
                return new WeatherDivinationByDate
                {
                    Date = startDate.AddDays(index),
                    TemperatureC = temperatureC,
                    Summary = summary
                };
            })
            .ToArray();

        return new WeatherForecasts(forecasts, analyst.Name);
    }

    private ForecastSummary GenerateSummary()
    {
        //return new Randomizer().PickItem(Summaries);
        return Summaries[Random.Shared.Next(Summaries.Length)];
    }


    private static int GenerateTemperature(ForecastSummary forecastSummary)
    {
        return forecastSummary switch
        {
            ForecastSummary.Freezing => Random.Shared.Next(-50, -20),
            ForecastSummary.Bracing => Random.Shared.Next(-20, -10),
            ForecastSummary.Chilly => Random.Shared.Next(-10, 5),
            ForecastSummary.Cool => Random.Shared.Next(5, 10),
            ForecastSummary.Mild => Random.Shared.Next(10, 20),
            ForecastSummary.Warm => Random.Shared.Next(20, 23),
            ForecastSummary.Balmy => Random.Shared.Next(23, 25),
            ForecastSummary.Hot => Random.Shared.Next(25, 30),
            ForecastSummary.Sweltering => Random.Shared.Next(30, 33),
            ForecastSummary.Scorching => Random.Shared.Next(33, 50),
            _ => throw new ArgumentOutOfRangeException(nameof(forecastSummary), forecastSummary, null)
        };
    }
}

public interface IRequireSomeAnalyst
{
    Task<Result<Analyst, AnalystError>> GetFirstFreeAnalyst();
}

public record Analyst(string Name);