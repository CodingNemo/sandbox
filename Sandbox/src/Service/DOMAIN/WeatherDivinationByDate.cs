using Core;

namespace Service.DOMAIN;

public enum ForecastSummary
{
    Freezing,
    Bracing,
    Chilly,
    Cool,
    Mild,
    Warm,
    Balmy,
    Hot,
    Sweltering,
    Scorching
}
public class WeatherDivinationByDate
{
    public DateTimeOffset Date { get; set; }

    public int TemperatureC { get; set; }

    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

    public ForecastSummary? Summary { get; set; }

    public int InvalidTemperatureC { get; set; }
}

public record WeatherForecasts(IReadOnlyCollection<WeatherDivinationByDate> DailyForecasts, string Analyst)
{
    public int NbOfDays => DailyForecasts.Count;
};

public abstract record ForecastsErrors : Error
{
    public record MissingForecastsError : ForecastsErrors;

    public static ForecastsErrors MissingForecasts() => new MissingForecastsError();

    public record TooManyDaysError(int Days, int Limit) : ForecastsErrors;

    public static ForecastsErrors TooManyDays(int days, int limit) => new TooManyDaysError(days, limit);

    public record AnalystForecastsError(AnalystError Error) : ForecastsErrors;

    public static ForecastsErrors AnalystError(AnalystError error) => new AnalystForecastsError(error);

}

public abstract record AnalystError : Error
{
    public record AnalystNotFoundError : AnalystError;

    public static AnalystError NotFound() => new AnalystNotFoundError();

    public record LazyAnalystError(string AnalystName) : AnalystError;

    public static AnalystError LazyAnalyst(string analystName) => new LazyAnalystError(analystName);

    public record UnexpectedError(Error Error) : AnalystError;

    public static AnalystError Unexpected(Error error) => new UnexpectedError(error);
}