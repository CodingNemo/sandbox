namespace Service.DOMAIN;

public interface IRequireNow
{
    public DateTimeOffset UtcNow { get; }
}