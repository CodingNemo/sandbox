﻿using System.Net;
using Core;
using Core.HttpClient;
using Service.DOMAIN;
using HttpRequest = Core.HttpClient.HttpRequest;

namespace Service.SPI
{
    public class AnalystHttpClient : IRequireSomeAnalyst
    {
        private readonly HttpClient _client;

        public AnalystHttpClient(HttpClient client)
        {
            _client = client;
        }
        public async Task<Result<Analyst, AnalystError>> GetFirstFreeAnalyst()
        {
            var get = HttpRequest.Get("/analyst");
            var analystResult = await _client.Run<Analyst>(get);

            return await analystResult!.Select(success =>
                success.StatusCode switch
                {
                    HttpStatusCode.OK => success.Content,
                    _ => throw new UnsupportedStatusCodeException(success.StatusCode, get)
                })
                .SelectError(async error =>
                    error switch
                    {
                        HttpCallErrors.HttpErrorsStatusErrors httpError =>
                            httpError.StatusCode switch
                            {
                                HttpStatusCode.Conflict => await ParseConflictError(httpError.ErrorContent) ?? AnalystError.Unexpected(error),
                                HttpStatusCode.NotFound => AnalystError.NotFound(),
                                _ => throw new UnsupportedStatusCodeException(httpError.StatusCode, get)
                            },
                        _ => AnalystError.Unexpected(error)
                    }
                );

        }

        private static async Task<AnalystError?> ParseConflictError(HttpContent content)
        {
            var conflictError = await content.ReadFromJsonAsync<AnalystConflictError>();

            return conflictError?.Analyst switch
            {
                not null => AnalystError.LazyAnalyst(conflictError.Analyst.Name),
                _ => null,
            };
        }

        private record AnalystConflictError(string? Message, Analyst? Analyst);
    }
}
