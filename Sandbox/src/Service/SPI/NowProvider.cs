using Service.DOMAIN;

namespace Service.SPI;

internal class NowProvider : IRequireNow
{
    public DateTimeOffset UtcNow => DateTimeOffset.UtcNow;
}