namespace Service.SPI;

public class Randomizer
{
    public T PickItem<T>(IEnumerable<T> items)
    {
        var index = Random.Shared.Next(items.Count());
        return items.ElementAt(index);
    }
}