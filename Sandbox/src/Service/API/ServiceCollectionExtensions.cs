﻿using Service.DOMAIN;
using Service.SPI;

namespace Service.API;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection RegisterWeatherServices(this IServiceCollection services)
    {
        services.AddTransient<WeatherForecastEngine>();
        services.AddSingleton<IRequireNow, NowProvider>();
        services.AddTransient<IRequireSomeAnalyst>(provider => provider.GetRequiredService<AnalystHttpClient>());
        return services;
    }
}
