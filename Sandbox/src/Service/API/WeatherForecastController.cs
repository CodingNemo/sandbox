using Core;
using Core.Controller;
using Microsoft.AspNetCore.Mvc;
using Service.DOMAIN;

namespace Service.API;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private readonly WeatherForecastEngine _engine;
    private readonly IRequireNow _nowProvider;

    public WeatherForecastController(
        WeatherForecastEngine engine,
        IRequireNow nowProvider)
    {
        _engine = engine;
        _nowProvider = nowProvider;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    public async Task<ActionResult<WeatherForecasts>> Get(int nbOfDays = 5)
    {
        var forecasts = await _engine.GetForecasts(nbOfDays, _nowProvider.UtcNow);

        return forecasts
            .ToActionResult()
            .OnError<ForecastsErrors.TooManyDaysError>(error => ActionResult.BadRequest<WeatherForecasts>(
                new ErrorPayload(ErrorCodes.TooManyDays, $"We could not process your request. The requested number of days, {error.Days}, is greater than the limit {error.Limit}")))
            .OnError<ForecastsErrors.MissingForecastsError>(_ => new NotFoundResult())
            .OnError<ForecastsErrors.AnalystForecastsError>(error => error.Error switch
            {
                AnalystError.LazyAnalystError lazyAnalystError => ActionResult.BadGateway<WeatherForecasts>(
                    new ErrorPayload(ErrorCodes.LazyAnalyst, $"Sorry, {lazyAnalystError.AnalystName} was too lazy to come to work today")),
                AnalystError.AnalystNotFoundError _ => ActionResult.BadGateway<WeatherForecasts>(new ErrorPayload(ErrorCodes.NoAnalystAvailable, "Sorry, there were no analyst available")),
                _ => ActionResult.InternalServerError<WeatherForecasts>()
            });

    }

    private static class ErrorCodes
    {
        public const string NoAnalystAvailable = "NOA";
        public const string LazyAnalyst = "LZY";
        public const string TooManyDays = "TMD";
    }

    private record ErrorPayload(string ErrorCode, string ErrorMessage);

    private static class ActionResult
    {
        public static ActionResult<T> BadGateway<T>(object payload) => new ObjectResult(payload)
        {
            StatusCode = StatusCodes.Status502BadGateway
        };

        public static ActionResult<T> BadRequest<T>(object payload) => new BadRequestObjectResult(payload);

        public static ActionResult<T> InternalServerError<T>() =>
            new StatusCodeResult(StatusCodes.Status500InternalServerError);
    }
}
